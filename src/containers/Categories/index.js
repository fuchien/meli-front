import React from 'react';

// COMPONENTS
import CategoriesUI from '../../components/Categories';

const Categories = ({ categories }) => <CategoriesUI categories={categories} />;

export default Categories;
