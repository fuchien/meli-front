import React from 'react';

// STYLED-COMPONENTS
import { Container } from './styles';

const Loader = () => (
    <Container>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </Container>
);

export default Loader;
